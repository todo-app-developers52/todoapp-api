### Node Express template project

[[_TOC_]]

## Project :tada:

The project is developed using Node.js. To get ready, install packages using NPM: 

```shell
npm install
```

A simple JavaScript call to launch the service can be written as: 

``` javascript
function launch() {
    // handle the server Launch.
}

app.start(1234, launch);
```

A Node.js app requires three things:

- [ ] NPM for package management
- [ ] Javascript for programming interface
- [x] Passion, and sometimes patience. :wink:

### Project structure

```mermaid
graph TD 
   A[JavaScript]  -->|Compiles down to| B(V8)
```
